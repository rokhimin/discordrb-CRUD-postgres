
module Bot
    module DiscordCommands
        module Contact
            extend Discordrb::Commands::CommandContainer
            class Message < ActiveRecord::Base
            end

            command(:delete, description: 'delete contact.',
                   usage: '>delete') do |event|

			    event.user.await(:breaks) do |breaks_event|
				    breaks = breaks_event.message.content.to_i

				    if breaks == breaks
		               		init = Contact.find(breaks)
							init.destroy
				            event.channel.send_embed do |embed| 
				            embed.colour = 0xff8040 
				            embed.add_field name: "Status", value: "Sukses ter-delete, silahkan cek ``>read``"
				           	end
					    else
				            event.channel.send_embed do |embed| 
				            embed.colour = 0xff8040 
				            embed.add_field name: "Status", value: "Gagal, silahkan coba lagi"
				           end
					      false
					    end
				  end
				  	t_id = Contact.pluck(:id)
				  	t_name = Contact.pluck(:name)
				  	t_phone = Contact.pluck(:phone)
		            event.channel.send_embed do |embed| 
		            embed.colour = 0xff8040 
	                embed.add_field name: "id", value: "#{t_id.join(" \n")}", inline: true
	                embed.add_field name: "name", value: "#{t_name.join(" \n")}", inline: true
		            embed.add_field name: "Status", value: "delete dengan command ``<id>`` yg ingin didelete"
		           end

            end

        end
    end
end