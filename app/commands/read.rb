
module Bot
    module DiscordCommands
        module Contact
            extend Discordrb::Commands::CommandContainer
            class Message < ActiveRecord::Base
            end

            command(:read, description: 'read contact.',
                   usage: '>read') do |event|
                t_name = Contact.pluck(:name)
                t_phone = Contact.pluck(:phone)
	            event.channel.send_embed do |embed| 
	            embed.colour = 0xff8040 
	            embed.add_field name: "Name", value: "#{t_name.join(" \n")}", inline: true
                embed.add_field name: "Phone", value: "#{t_phone.join(" \n")}", inline: true
	            embed.add_field name: "Status", value: "contact dari database telah di load"
	           end
            end


        end
    end
end