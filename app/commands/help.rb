module Bot
    module DiscordCommands
        module Help
            extend Discordrb::Commands::CommandContainer
            command(:help) do |event|
                servers = event.bot.servers 
                event.channel.send_embed do |embed| 
                embed.colour = 0xff8040 
                embed.add_field name: "Info", value: "`>about`" 
                embed.add_field name: "Commands", value: "`>create`| `>read` | `>update` | `>delete`" 
                end
            end

        end
    end
end