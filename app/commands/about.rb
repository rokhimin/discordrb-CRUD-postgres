
module Bot
    module DiscordCommands
        module About
            extend Discordrb::Commands::CommandContainer
            command(:about) do |event|
                servers = event.bot.servers
                event.channel.send_embed do |embed|
                    embed.colour = 0xff8040 
                    embed.title = '**Rokhimin-69**'
                    embed.add_field name: "Developer :", value: "twitter.com/rokhiminwahid | twitter.com/whdzera"
                    embed.add_field name: "Written :", value: "RubyLanguage"
                    embed.add_field name: "Library :", value: "discordrb"
                    embed.add_field name: "Link :", value: "https://github.com/rokhimin/discordbot-ruby-postgresql"
                end
            end

        end
    end
end
