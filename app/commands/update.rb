
module Bot
    module DiscordCommands
        module Contact
            extend Discordrb::Commands::CommandContainer
            class Message < ActiveRecord::Base
            end
            
            command(:update, description: 'update contact.',
                   usage: '>update') do |event|

			  
			    event.user.await(:breaks) do |breaks_event|
				    breaks = breaks_event.message.content
				    s_id = breaks.match(/./).to_a
               		s_name = breaks.gsub(/[0-9]/, "")
               		s_phone = breaks.gsub(/[A-Za-z]/, "")
               		s2_phone = s_phone.gsub(/.  /, "")

				    if breaks == breaks
		               		init = Contact.find(s_id.join.to_i)
							init.update(:name => s_name, :phone => s2_phone)
							init.save
				            event.channel.send_embed do |embed| 
				            embed.colour = 0xff8040 
				            embed.add_field name: "Status", value: "Sukses ter-update, silahkan cek ``>read``"
				           	end
					    else
				            event.channel.send_embed do |embed| 
				            embed.colour = 0xff8040 
				            embed.add_field name: "Status", value: "Gagal, silahkan coba lagi"
				           end
					      false
					    end
				  end
				  	t_id = Contact.pluck(:id)
				  	t_name = Contact.pluck(:name)
				  	t_phone = Contact.pluck(:phone)
		            event.channel.send_embed do |embed| 
		            embed.colour = 0xff8040 
	                embed.add_field name: "id", value: "#{t_id.join(" \n")}", inline: true
	                embed.add_field name: "name", value: "#{t_name.join(" \n")}", inline: true
		            embed.add_field name: "Status", value: "update dengan command ``<id> <name> <phone>``"
		           end

            end


        end
    end
end