
module Bot
    module DiscordCommands
        module Contact
            extend Discordrb::Commands::CommandContainer
            class Contact < ActiveRecord::Base
            end

            command(:create, description: 'create contact.', min_args: 1,
                   usage: '>create <name> <phone>') do |event, *query|
                from = query.join(' ')
			    temp = ''
			    temp << from
               	t_name = temp.gsub(/[0-9]/, "")
               	t_phone = temp.gsub(/[A-Za-z]/, "")
				init = Contact.new( :name  => t_name,
				                      :phone => t_phone)
				init.save

                event.channel.send_embed do |embed| 
                embed.colour = 0xff8040 
                embed.add_field name: "Created", value: "#{t_name} | #{t_phone}"
                embed.add_field name: "Status", value: "Kontak baru sdh dibuat, cek ``>read``"
                end
            end

        end
    end
end
