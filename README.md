![Lang](https://img.shields.io/badge/Language-Ruby-red)
![db](https://img.shields.io/badge/db-PostgreSql-yellow)
![Coverage Status](https://img.shields.io/badge/coverage-99%25-green)

# discordbot ruby postgresql
discordrb + postgresql Integration | +example CRUD 

## Info
Bot Contact Phone, CRUD(Create, Read, Update, Delete) with postgresql database

## Require
1. Ruby
2. PostgreSQL DB 

## Structure Table
###### Contact => ::[id]::,::[name]::,::[phone]::

## Setup
- set configuration, db, bot ,etc 
```config/config.rb```
- install dependencies
```bundle install```
- create table db
```rake db:migrate```

## Test
Test connection Postgresql
```ruby test.rb```

## Run Bot
```ruby linkstart.rb```

## ss
![](https://i.imgur.com/t29C6V6.jpg)
![](https://i.imgur.com/oY9RTnA.jpg)
![](https://i.imgur.com/EKhpAjL.jpg)

## License
MIT License.


