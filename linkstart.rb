require 'discordrb'
require 'configatron'
require 'json'
require_relative 'config/config.rb'

module Bot

    bot = Discordrb::Commands::CommandBot.new token: configatron.token, prefix: configatron.prefix

    module DiscordCommands; end
    Dir['app/commands/*.rb'].each { |mod| load mod }
    DiscordCommands.constants.each do |mod|
    bot.include! DiscordCommands.const_get mod
    end

    module DiscordEvents; end
    Dir['app/events/*.rb'].each { |mod| load mod }
    DiscordEvents.constants.each do |mod|
    bot.include! DiscordEvents.const_get mod
    end

    bot.run

end

